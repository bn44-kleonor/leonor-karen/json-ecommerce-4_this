<?php
    require "partials/header.php";
?>

<!-- ADD PRODUCT -->
<div class="container">
    <div class="row">
        <div class="col-6 offset-6">
            <div class="card">
                <div class="card-body">
                    <form method="POST" action="assets/lib/processCreateProduct.php" enctype="multipart/form-data">
                        <div class="form-group">
                            <label for="name">Product Name</label>
                            <input type="text" class="form-control" name="name" id="name" aria-describedby="name" required>
                        </div>
                        <div class="form-group">
                            <label for="price">Price</label>
                            <input placeholder="1.0" step="0.01" min="0" max="10" class="form-control" name="price" id="price" aria-describedby="price" required>
                        </div>
                        <div class="form-group">
                            <label for="description">Description</label>
                            <textarea class="form-control" name="description" id="description" rows="3" required></textarea>
                        </div>
                        <div class="form-group">
                            <label for="image">Image</label>
                            <input type="file" class="form-control-file" name="image" id="image" required>
                        </div>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
    require "partials/footer.php";
?>

