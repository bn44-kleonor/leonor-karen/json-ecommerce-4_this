<?php
	require "partials/header.php";
?>
	<style type="text/css">
		img {
			height: 280px;
		}
	</style>
	<!-- alert div, nidelete -->
		<div class="row">
			<?php 
			for($i = 0; $i < count($products); $i++){	//to display the items from products.json
			?>
			<div class="col col-md-4 col-lg-3 card-group mb-4">
				<div class="card">
				<a href="product.php?productid=<?php echo $i; ?>">	<!--so when clicked, it will go to detailed page of the product-->
					<img class="card-img-top" src="assets/lib/<?php echo $products[$i]["image"]; ?>" alt="Card image cap">
					<div class="card-body">
					    <h5 class="card-title">
					    	<?php echo $products[$i]["name"]; ?>
					    </h5>
					    <p class="card-text text-secondary">
					    	&#8369;
					    	<?php echo number_format($products[$i]["price"], 2, ".", ""); ?>
					    </p>
					    <p class="card-text">
					    	<?php echo $products[$i]["description"]; ?>
					    </p>
					</div>
				</a>
				<div class="card-footer">
				  	<form method="POST" 
				    action="assets/lib/processAddToCart.php?productid=<?php echo $i; ?>">		<!--so when user adds to cart, it will call processAddToCart.php-->
					    <input type="number" min="1" name="quantity" class="form-control" required>
					    <button class="btn btn-primary btn-block">Add To Cart</button>
					</form>
				</div>
				</div>
			</div>
			<?php } ?>
		</div>
	</div>
<?php
	require "partials/footer.php";
?>