<?php
	session_start();	//to access session and manipulate variables
	session_unset();	//unsets all of the session variable that you have created
	session_destroy();	//to destroy your session. when logged out, it should return to login page or index.php page. The cart will show empty again.
	header("Location: ../../login.php");	//where to go after logging out
?>