<?php

	session_start();
	$product_name = $_POST["name"];	//means: get the value(POST) of the html element with name "name"
	$price = $_POST["price"];
	$price = floatval($price);
	$desc = $_POST["description"];


	//SYNTAX: $_FILES[name in the form][property]
	// commonly used properties: name, temp_name, size
	var_dump($price);
	echo "</br>";
	var_dump($_FILES["image"]);
	echo "</br>";
	var_dump($_FILES["image"]);
	var_dump($_FILES["image"]["name"]);
	var_dump($_FILES["image"]["tmp_name"]);

	//echo "pre"
	var_dump($_FILES["image"]["size"]);


	$filename = $_FILES["image"]["name"];
	$file_tmpname = $_FILES["image"]["tmp_name"];
	$filesize = $_FILES["image"]["size"];


	//var_dump(pathinfo($filename, PATHINFO_EXTENSION));
	$file_type = strtolower(pathinfo($filename, PATHINFO_EXTENSION));
	//$path = pathinfo($filename);
	//var_dump($path["extension"]);

	//validation
	$hasDetails = false;
	$isImg = false;

	if($product_name != "" && $price > 0 && $desc != ""){
		$hasDetails = true;
	}

	if($file_type == "jpg" || $file_type == "png" || $file_type == "jpeg"){
		$isImg = true;
	}

	if($filesize > 0 && $isImg && $hasDetails ){
		//var_dump($file_tmpname);

		$final_path = "images/" . $filename;
		move_uploaded_file($file_tmpname, $final_path);
		//exit();
		//move temporarily stored image to final file path
	} else {
		//create error message: Please upload an image
		$_SESSION["error_message"] = "Please upload an image";
		//redirect to addproduct.php
		header("Location: ../../addproduct.php");
	}


	//create a php associative array
	$new_product = [
		"name" => $product_name,
		"price" => $price,
		"description" => $desc,
		"image" => $final_path
	];

	//access contents of products.json
	$products_objects = file_get_contents('products.json');

	//convert contents to php array
	$products = json_decode($products_objects, true);

	//push new product to products.json
	array_push($products, $new_product);

	//open json file for writing
	$to_write = fopen('products.json', 'w');

	//write to the opened json file
	fwrite($to_write, json_encode($products, JSON_PRETTY_PRINT));

	//close json file
	fclose($to_write);

	$_SESSION["message"] = "$product_name has been successfully added!";
	header("Location: ../../products.php");
?>
