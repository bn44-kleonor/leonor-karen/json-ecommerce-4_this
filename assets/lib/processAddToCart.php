<?php
	session_start();

	$quantity = $_POST["quantity"];
	$product_id = $_GET["productid"];

	//SETTING A SESSION
	// SYNTAX: $_SESSION["cart"] = "value";

	if(isset($_SESSION["cart"][$product_id])){
		$_SESSION["cart"][$product_id] += $quantity; 
	} else {
		$_SESSION["cart"][$product_id] = $quantity;
	}
	
	$_SESSION["message"] = "Product has been added to cart!";
	//var_dump($_SERVER["HTTP_REFERER"]);
	if($_SERVER["HTTP_REFERER"]){
		$referer = ($_SERVER["HTTP_REFERER"]);
		if(strpos($referer, "product.php")){
			header("Location: ../../product.php?productid={$product_id}");
			exit();
		} else {
			header("Location: ../../index.php");
			exit();
		}
	} else {
		header("Location: ../../index.php");
	}
	//assign value to $referer
	//use strpos to check if $referer contains "product.php"
	//header("Location: ../../index.php");
	//exit();
?>