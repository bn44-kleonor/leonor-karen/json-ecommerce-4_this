<?php 

var_dump($_FILES);
var_dump($_FILES["new_image"]);
var_dump($_POST["name"]);
var_dump($_POST["description"]);
var_dump($_POST["price"]);

?>


<?php

	session_start();
	$product_name = $_POST["name"];	//means: get the value(POST) of the html element with name "name"
	$price = $_POST["price"];
	$price = floatval($price);
	$desc = $_POST["description"];

	//access products.json
	$products_objects = file_get_contents("products.json");
	$products = json_decode($products_objects, true);
	$i = $_GET["productid"];


	//validation
	$hasDetails = false;
	$isImg = false;

	if($product_name != "" && $price > 0 && $desc != ""){
		$hasDetails = true;
	}

	if($_FILES["new_image"]["size"] != 0 ){
		//SYNTAX: $_FILES[name in the form][property]
		// commonly used properties: name, temp_name, size
		$filename = $_FILES["new_image"]["name"];
		$file_tmpname = $_FILES["new_image"]["tmp_name"];
		$filesize = $_FILES["new_image"]["size"];

		//var_dump(pathinfo($filename, PATHINFO_EXTENSION));
		$file_type = strtolower(pathinfo($filename, PATHINFO_EXTENSION));
		//$path = pathinfo($filename);
		//var_dump($path["extension"]);

		if($file_type == "jpg" || $file_type == "png" || $file_type == "jpeg"){
			$isImg = true;
		}

		if($filesize > 0 && $isImg && $hasDetails ){
			//var_dump($file_tmpname);
			$final_path = "images/" . $filename;
			move_uploaded_file($file_tmpname, $final_path);
			//exit();
			//move temporarily stored image to final file path
		} else {
			//create error message: Please upload an image
			$_SESSION["error_message"] = "Please upload an image";
			//redirect to addproduct.php
			header("Location: ../../addproduct.php");
		}

	} else {
		$final_path = $products[$i]["image"];
	}

		//update properties of product
		$products[$i]["name"] = $product_name;
		$products[$i]["price"] = $price;
		$products[$i]["description"] = $desc;
		$products[$i]["image"] = $final_path;


		//open json file for writing
		$to_write = fopen('products.json', 'w');

		//write to the opened json file
		fwrite($to_write, json_encode($products, JSON_PRETTY_PRINT));

		//close json file
		fclose($to_write);

		$_SESSION["message"] = "$product_name has been successfully updated!";
		header("Location: ../../editproduct.php?productid={$i}");

?>
