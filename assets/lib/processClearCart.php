<?php 
	session_start();
	unset($_SESSION["cart"]);

	$_SESSION["message"] = "Cart has been cleared!";

	header("Location: ../../index.php");

?>