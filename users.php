<!-- USERS -->
<div class="container mb-5">
    <div class="row">
        <div class="col">
            <table class="table">
                <thead class="thead-dark">
                    <tr>
                        <th scope="col" width="15%">#</th>
                        <th scope="col" width="20%">First Name</th>
                        <th scope="col" width="20%">Last Name</th>
                        <th scope="col" width="10%">Email</th>
                        <th scope="col" width="10%">Role</th>
                        <th scope="col" width="25%">Actions</th>
                    </tr>
                </thead>
                <tbody>
                
                
                    <tr>
                        <td>1</td>
                        <td>John</td>
                        <td>Snow</td>
                        <td>snow@gmail.com</td>
                        <td>Admin</td>
                        <td>
                            <div class="d-flex flex-row">
                                <!-- READ -->
                                <a href="#" class="btn btn-primary mr-1">View</a>

                                <!-- UPDATE -->
                                <a href="#" class="btn btn-warning mr-1">Edit Role</a>

                                <!-- DELETE -->
                                <a href="#" class="btn btn-danger mr-1">Delete</a>
                            </div>
                        </td>
                    </tr>
                    
                    
                </tbody>
            </table>
        </div>
    </div>
</div>