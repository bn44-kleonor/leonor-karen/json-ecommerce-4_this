<?php
	require "partials/header.php";
	$i = $_GET["productid"];
?>

<!-- PRODUCT -->
<div class="container">
	<div class="row">
		<div class="col-md-1"></div>
		<!-- IMAGE -->
		<div class="col-md-3">
			<div class="card">
				<img src="assets/lib/<?php echo $products[$i]["image"]; ?>" class="card-img-top" alt="product-image">
			</div>
		</div>
		<!-- DETAILS -->
		<div class="col-md-7">
			<div class="card">
				<div class="card-body">
				    <h5 class="card-title">
				    	<?php echo $products[$i]["name"]; ?>
				    </h5>
				    <p class="card-text text-secondary">
				    	&#8369;
				    	<?php echo number_format($products[$i]["price"], 2, ".", ""); ?>
				    </p>
				    <p class="card-text">
				    	<?php echo $products[$i]["description"]; ?>
				    </p>
				</div>
				<div class="card-footer">
				  	<form method="POST" 
				    action="assets/lib/processAddToCart.php?productid=<?php echo $i; ?>">		<!--so when user adds to cart, it will call processAddToCart.php-->
					    <input type="number" min="1" name="quantity" class="form-control" required>
					    <button class="btn btn-primary btn-block">Add To Cart</button>
					</form>
				</div>



			</div>
		</div>
		<div class="col-md-1"></div>
	</div>	
</div>

<?php
	require "partials/footer.php";
?>
