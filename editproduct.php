<?php
    require "partials/header.php";
    $i = $_GET["productid"];
?>

<!-- EDIT PRODUCT -->
<form method="POST" action="assets/lib/processUpdateProduct.php?productid=<?php echo $i; ?>" class="d-flex flex-row" enctype="multipart/form-data">
    <div class="container">
        <div class="row">
            <div class="col-6">
                <img src="assets/lib/<?php echo $products[$i]["image"]; ?>" alt="Card image">
            </div>
            <div class="col-6">
                <div class="form-group">
                    <label for="new_image">Image</label>
                    <input type="file" class="form-control-file" name="new_image" id="new_image">
                </div>
                <div class="form-group">
                    <label for="name">Product Name</label>
                    <input type="text" class="form-control" name="name" id="name" required value="<?php echo $products[$i]["name"]; ?>">
                </div>
                <div class="form-group">
                    <label for="price">Price</label>
                    <input type="number" placeholder="1.0" step="0.01" min="0" class="form-control" name="price" id="price" required value="<?php echo $products[$i]["price"]; ?>">
                </div>
                <div class="form-group">
                    <label for="description">Description</label>
                    <textarea class="form-control" name="description" id="description" rows="3" required><?php echo $products[$i]["description"]; ?></textarea>
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
                <a class="btn btn-secondary" href="products.php">Back</a>
            </div>
        </div>
    </div>
</form>

<?php
    require "partials/footer.php";
?>