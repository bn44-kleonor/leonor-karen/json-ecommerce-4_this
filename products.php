<?php
	require "partials/header.php";
	//var_dump($_SESSION);
	//var_dump(isset($_SESSION["isAdmin"]);	//false if not logged in
	//isset($_SESSION["isAdmin"] only checks if the variable 'isAdmin' is set and accessible
	//die();

	//var_dump(!isset($_SESSION["isAdmin"]) || $_SESSION["isAdmin"] == false)
	//var_dump($_SESSION["isAdmin"] == false);
	//die();

	if(!isset($_SESSION["isAdmin"]) || $_SESSION["isAdmin"] == false){
		header("Location: index.php");
	}

?>


<!-- ADMIN -->
<div class="container">
	<div class="row">
		<div class="col">
			<table class="table">
				<thead class="thead-dark">
				    <tr>
				      <th scope="col">#</th>
				      <th scope="col">Name</th>
				      <th scope="col">Price</th>
				      <th scope="col">Action</th>
				    </tr>
				</thead>
				<tbody>
					<?php 
						for($i = 0; $i<count($products); $i++){
					?>
				    <tr>
				    	<td><?php echo $i + 1; ?></td>
						<th scope="row"><?php echo $products[$i]["name"]; ?></th>
						<td><?php echo $products[$i]["price"]; ?></td>
						<td>
							<!-- READ -->
							<a href="product.php?productid=<?php echo $i; ?>">
								<button type="button" class="btn btn-primary">View</button>
							</a>

							<!-- UPDATE -->
							<a href="editproduct.php?productid=<?php echo $i; ?>">
								<button type="button" class="btn btn-warning">Edit</button>
							</a>

							<!-- DELETE -->
							<a href="deleteproduct.php?productid=<?php echo $i; ?>">
								<button type="button" class="btn btn-danger">Delete</button>
							</a>
						</td>
				    </tr>
				    <?php } ?>
				</tbody>
			</table>
		</div>
	</div>
    <div class="row">
        <div class="col">
            <a href="addproduct.php" class="btn btn-success">Add Product</a>
        </div>
    </div>
</div>

<?php
	require "partials/footer.php";
?>

