<?php
	require "partials/header.php";
	//var_dump($_SESSION);
	//die();
?>

<!-- CART -->
<div class="container">
	<div class="row">
		<div class="col">
			<form method="POST" action="assets/lib/processClearCart.php">
				<button class="btn btn-danger">
					Clear Cart
				</button>
			</form>
		</div>		
	</div>

	<div class="row">
		<div class="col">
			<table class="table">
				<thead class="thead-dark">
				    <tr>
				      <th scope="col">Item</th>
				      <th scope="col">Price</th>
				      <th scope="col">Quantity</th>
				      <th scope="col">Sub-Total</th>
				    </tr>
				</thead>
				<tbody>
					<?php 
					$total = 0;
						for($i = 0; $i<count($products); $i++){
							if(isset($_SESSION["cart"][$i])){
								$total += ($_SESSION["cart"][$i] * $products[$i]["price"]); }
					?>
				    <tr>
						<th scope="row"><?php echo $products[$i]["name"]; ?></th>
						<td><?php echo $products[$i]["price"]; ?></td>
						<td><?php echo $_SESSION["cart"][$i]; ?></td>
						<td><?php echo $_SESSION["cart"][$i] * $products[$i]["price"]; ?></td>
				    </tr>
				    <?php } ?>
				    <tr>
				    	<th scope="row" colspan="3">Total: </th>
				    	<td><?php echo $total; ?></td>
				    </tr>
				</tbody>
			</table>
		</div>
	</div>

</div>

<?php
	require "partials/footer.php";
?>

