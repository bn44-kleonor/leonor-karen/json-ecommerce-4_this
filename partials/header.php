<!DOCTYPE html>
<html>
<?php
	session_start();
	//GET OBJECTS FROM PRODUCTS.JSON
	$product_objects = file_get_contents("assets/lib/products.json");
	//CONVERT TO PHP ARRAY
	$products = json_decode($product_objects, true);
	//var_dump($_SESSION);
	//echo "</br>";
	//var_dump($_SESSION["isAdmin"]);
?>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="IE=Edge">
	<title>PHP JSON ECOMMERCE</title>
	<script src="https://kit.fontawesome.com/02db094c22.js"></script>
	<link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="assets/css/style.css">
</head>

<body>
	<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
		<div class="container">
			<a class="navbar-brand" href="index.php"><i class="fas fa-store-alt"></i>Shop.io</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarResponsive">
				<ul class="navbar-nav ml-auto">
					<!-- Insert Links Here -->

					<?php if(isset($_SESSION["isAdmin"]) && $_SESSION["isAdmin"] == false ) { ?>
						<li class="nav-item">
							<a class="nav-link" href="cart.php"><i class="fas fa-shopping-cart"></i> Cart
								<?php 
									if(isset($_SESSION["cart"])){
										echo "<span class='badge badge-secondary'>" . count($_SESSION['cart']) . "</span>";
									}
								?>
							</a>
						</li>
					<?php } ?>


					<!-- IF NOT LOGGED IN -->
					<?php if(!isset($_SESSION["email"])){ ?>
					<li class="nav-item">
						<a class="nav-link" href="login.php"><i class="fas fa-lock"></i> Login</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="register.php"><i class="far fa-user"></i> Register</a>
					</li>
					<?php } else { ?>
					<li class="nav-item">
						<a class="nav-link" href="assets/lib/processLogout.php"><i class="fas fa-shopping-cart"></i> Logout</a>
					</li>
					<?php } ?>

					<!-- IF LOGGED IN AS ADMIN -->
					<?php if(isset($_SESSION["isAdmin"]) && $_SESSION["isAdmin"] == true ) { ?>
						<li class="nav-item">
							<a class="nav-link" href="products.php"></i> Products</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="users.php"></i> Users</a>
						</li>
					<?php } ?>
				</ul>
			</div>
		</div>
	</nav>
	<!-- ALERT MESSAGE -->
	<div class="row">
		<div class="col" id="message_container">
			<?php 
				if(isset($_SESSION["message"])){
			?>
				<div class='alert alert-success alert-dismissible fade show' role='alert' id='success_message'>
				 		<?php echo $_SESSION["message"]; ?>
				</div>
			<?php
				unset($_SESSION["message"]);
				// header("Refresh:2");
				} elseif (isset($_SESSION["error_message"])){
			?>
				<div class='alert alert-danger alert-dismissible fade show' role='alert' id='error_message'>
				 		<?php echo $_SESSION["error_message"]; ?>
				</div>
			<?php 
				unset($_SESSION["error_message"]);
				} 
			?>
		</div>
	</div>
